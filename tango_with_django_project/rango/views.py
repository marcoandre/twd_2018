from django.http import HttpResponse

def index(request):
    return HttpResponse("Rango está vivo!")

def about(request):
    return HttpResponse("About do Rango!")

def help(request):
    return HttpResponse("Help do Rango!")

def bsi(request):
    return HttpResponse("BSI!")
